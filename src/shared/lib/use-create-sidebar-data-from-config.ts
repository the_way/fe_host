import type { TFunction } from "react-i18next";
import { useTranslation } from "react-i18next";

import type { TSidebarConfig, TSidebarData } from "shared/config";

const createSidebarDataFromConfig = (
  dataArray: TSidebarConfig[],
  t: TFunction
): TSidebarData[] =>
  dataArray.map((value: TSidebarConfig) => {
    const { menuItems, menuTitle } = value;

    return {
      menuItems: menuItems?.map((menuItem) => ({
        children: t(menuItem.translationKey),
        ...menuItem,
      })),
      menuTitle: {
        text: t(menuTitle.translationKey),
        ...menuTitle,
      },
    };
  });

export const useCreateSidebarDataFromConfig = (
  dataArray: TSidebarConfig[]
): TSidebarData[] => {
  const { t, i18n } = useTranslation("main", {
    keyPrefix: "shared.molecules.sidebar",
  });
  console.log("i18n.language", i18n.language);
  console.log("t", t);
  console.log("i18n.language123", t("about.cv"));
  return createSidebarDataFromConfig(dataArray, t);
};
