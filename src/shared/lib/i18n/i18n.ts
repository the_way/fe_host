import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import { enI18nConfig, ruI18nConfig } from "shared/config";

// eslint-disable-next-line @typescript-eslint/no-floating-promises
i18n.use(initReactI18next).init({
  fallbackLng: "en",
  interpolation: {
    escapeValue: false,
  },
  resources: {
    ru: { main: ruI18nConfig },

    en: {
      main: enI18nConfig,
    },
  },
});

export default i18n;
