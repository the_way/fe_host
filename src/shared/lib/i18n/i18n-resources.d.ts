import "react-i18next";

declare module "react-i18next" {
  export interface Resources {
    main: typeof import("../../config/i18n/ru.json");
  }
}
