import { ROUTES } from "shared/config";

export type NestedKeyOf<TObject extends object> = {
  [Key in keyof TObject & string]: TObject[Key] extends object
    ? // предупреждение о возможной бесконечной вложенности
      // @ts-ignore
      `${Key}.${NestedKeyOf<TObject[Key]>}`
    : Key;
}[keyof TObject & string];

type TRoutes = typeof ROUTES;

type TNested = {
  INDEX: string;
  [key: string]: string | TNested;
};

type TRoutesTemplate = {
  [key: string]: string | TNested;
};

const getRouteForUrl =
  (obj: typeof ROUTES extends TRoutesTemplate ? typeof ROUTES : never) =>
  <T extends NestedKeyOf<TRoutes>>(path: T) => {
    let idx = 0;
    const keysArray = path.split(".");
    const iterateRoutes = (
      nestedRoute: TRoutesTemplate[keyof TRoutesTemplate],
      url: string
    ): string => {
      if (keysArray[idx] === "INDEX") {
        return url;
      }
      if (typeof nestedRoute !== "string") {
        idx += 1;
        const nestedIndex =
          nestedRoute.INDEX[0] === "/"
            ? nestedRoute.INDEX
            : `/${nestedRoute.INDEX}`;
        const newUrl = `${url}${nestedIndex}`;
        return iterateRoutes(nestedRoute[keysArray[idx]], newUrl);
      }
      const value = nestedRoute[0] === "/" ? nestedRoute : `/${nestedRoute}`;
      return `${url}${value}`;
    };
    return iterateRoutes(obj[keysArray[idx]], "");
  };

export const getUrl = getRouteForUrl(ROUTES);
