import type { ROUTES } from "shared/config";

import { getUrl } from "./get-url";
import type { NestedKeyOf } from "./get-url";

const addParamsToUrl = (url: string, param: string) => url + param;

type TRoutes = typeof ROUTES;

export const getUrlWithEndParams = (
  route: NestedKeyOf<typeof ROUTES>,
  param: string
) => addParamsToUrl(getUrl(route), param);

// TODO: (AM) use or clear
// const getParamsQuantity = (path: NestedKeyOf<typeof ROUTES>) => {
//   const url = getUrl(path);
//   return url.split("id").length - 1;
// };

const replaceIdWithParam = (url: string, param: string) => {
  const idx = url.indexOf(":id");
  const firstSlice = url.slice(0, idx);
  const secondSlice = idx + 4 >= url.length ? "" : url.slice(idx + 4);
  return firstSlice + param + secondSlice;
};

type TParams = {
  id: string;
};

export const getUrlWithParams = <T extends NestedKeyOf<TRoutes>>(route: T) => {
  return (params: TParams) => {
    let unfinishedUrl = getUrl(route);
    const replaceAllIdsWithParams = (): string => {
      const paramsKey = "id";
      const param = params[paramsKey];
      if (param === undefined) {
        throw new Error(
          "Вместо строкового значения параметра был передан undefined"
        );
      }
      return replaceIdWithParam(unfinishedUrl, param);
    };
    return replaceAllIdsWithParams();
  };
};

// work copy
// export const getUrlWithParams = (
//   route: NestedKeyOf<typeof ROUTES>,
//   params: { id1?: string; id2?: string; id3?: string; id4?: string; id5?: string },
// ): string => {
//   const q = getParamsQuantity(route);
//   if (q > Object.keys(params).length) {
//     throw new Error(`Передано недостаточное количество аргументов (${route})`);
//   } else if (q < Object.keys(params).length) {
//     throw new Error(`Передано слишком большое количество аргументов (${route})`);
//   }
//   let unfinishedUrl = getUrl(route);
//   let counter = 1;
//   const replaceAllIdsWithParams = (): string => {
//     const paramsKey = `id${counter}` as 'id1' | 'id2' | 'id3' | 'id4' | 'id5';
//     const param = params[paramsKey];
//     if (param === undefined) {
//       throw new Error('Вместо строкового значения параметра был передан undefined');
//     }
//     unfinishedUrl = replaceIdWithParam(unfinishedUrl, param);
//     counter += 1;
//     if (counter <= q) {
//       return replaceAllIdsWithParams();
//     }
//     return unfinishedUrl;
//   };
//   return replaceAllIdsWithParams();
// };
// getUrlWithParams('TEST.ID1.NESTED_CHILD.ID2')({'id2'})
