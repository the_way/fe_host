export const openLink = (url: string, inNewTab = true) => {
  const target = inNewTab ? "__blank" : "";
  window.open(url, target);
};
