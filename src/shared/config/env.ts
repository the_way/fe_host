export const ENV = {
  BASE_API:
    (process.env.NODE_ENV === "development"
      ? process.env.DEV_API
      : process.env.PROD_API) || ("" as string),
  BE_MAIN_PORT: process.env.BE_MAIN_PORT || 8081,
  FE_SHARED_UI_PORT: process.env.FE_SHARED_UI_PORT || 8082,
  FE_AUTH_PORT: process.env.FE_AUTH_PORT || 8083,
  FE_HOST_PORT: process.env.FE_HOST_PORT || 8084,
};
