export const ROUTES = {
  AUTH: "/authentication",
  HOME: "/home",
  TESTS: {
    INDEX: "/tests",
    CREATE: "create",
    EDIT: "edit",
    TAKE: "take",
  },
  USERS: "users",
  ABOUT: {
    INDEX: "/about",
    CV: "cv",
    PORTFOLIO: "portfolio",
    SKILLS: "skills",
  },
} as const;
