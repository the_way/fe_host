import { getUrl } from "shared/lib";
console.log('LINKS');
export const LINKS = {
  AUTH: "/authentication",
  HOME: "/home",
  TESTS: {
    INDEX: "/tests",
    CREATE: getUrl("TESTS.CREATE"),
    EDIT: getUrl("TESTS.EDIT"),
    TAKE: getUrl("TESTS.TAKE"),
  },
  USERS: "users",
  ABOUT: {
    INDEX: "/about",
    CV: getUrl("ABOUT.CV"),
    PORTFOLIO: getUrl("ABOUT.PORTFOLIO"),
    SKILLS: getUrl("ABOUT.SKILLS"),
  },
} as const;

console.log("LINKS", LINKS);
