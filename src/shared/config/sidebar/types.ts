import type { TFuncKey } from "react-i18next";
import type { TLinkProps } from "shared_ui/remote";
import type { TSidebarItemProps } from "shared_ui/src/shared/ui/organisms/sidebar/types";

// export type TLinkConfig = {
//   translationKey: TFuncKey;
// } & Pick<TLinkProps, "to">;

// type TConfigData = TLinkConfig;

// export type TSidebarConfig =
//   | TLinkConfig & {
//       data?: TConfigData[];
//     };

// type TDataTemplate<T> = {
//   type: T;
//   text: string;
// };

// //  {pathname, type: 'link', isNavigationLink }
// type TLinkData = Omit<TLinkConfig, "translationKey" | "data"> &
//   TDataTemplate<"link"> & { isNavigationLink?: true };

// export type TData = TLinkData;

// export type TSidebarData = TData & { data?: TData[] };

export type TSidebarConfig = {
  menuTitle: Omit<TSidebarItemProps["menuTitle"], "text"> & {
    translationKey: TFuncKey;
  };
  menuItems?: (Omit<TLinkProps, "children"> & {
    translationKey: TFuncKey;
  })[];
};
