import { LINKS, ROUTES } from "shared/config";
import BadgeIcon from "@mui/icons-material/Badge";
import type { TSidebarConfig } from "./types";
import HomeIcon from "@mui/icons-material/Home";
import LayersIcon from "@mui/icons-material/Layers";

export const SIDEBAR: TSidebarConfig[] = [
  {
    menuTitle: {
      translationKey: "index",
      to: ROUTES.ABOUT.INDEX,
      Icon: <HomeIcon />,
    },
  },
  {
    menuTitle: {
      translationKey: "features.index",
      to: ROUTES.TESTS.INDEX,
      Icon: <LayersIcon />,
    },
    menuItems: [
      {
        translationKey: "features.testing",
        to: ROUTES.TESTS.INDEX,
      },
    ],
  },
  {
    menuTitle: {
      translationKey: "about.index",
      to: ROUTES.ABOUT.INDEX,
      Icon: <BadgeIcon />,
    },
    menuItems: [
      {
        translationKey: "about.cv",
        to: LINKS.ABOUT.CV,
      },
      {
        translationKey: "about.skills",
        to: LINKS.ABOUT.SKILLS,
      },
      {
        translationKey: "about.portfolio",
        to: LINKS.ABOUT.PORTFOLIO,
      },
    ],
  },
];
