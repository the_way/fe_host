import en from "./en.json";
import ru from "./ru.json";

export { ru as ruI18nConfig, en as enI18nConfig };
