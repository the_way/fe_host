import { baseApi } from "auth/remote";

export const feedApi = baseApi.injectEndpoints({
  endpoints: (build) => ({
    add: build.mutation({
      query: (body) => ({
        url: "feed/add",
        method: "POST",
        body,
      }),
    }),
    edit: build.mutation({
      query: (body) => ({
        url: "feed/edit",
        method: "PUT",
        body,
      }),
    }),
    remove: build.mutation({
      query: (body) => ({
        url: "feed/delete",
        method: "DELETE",
      }),
    }),
    getAll: build.query({
      query: () => ({
        url: "feed/getAll",
        method: "GET",
      }),
    }),
  }),
});

export const {
  useAddMutation,
  useEditMutation,
  useGetAllQuery,
  useRemoveMutation,
} = feedApi;
