import { AuthPage, setAuthorized, AuthOutlet } from "auth/remote";
import { memo } from "react";
import { Routes, Route } from "react-router-dom";

import { ROUTES } from "shared/config";

import { MainTemplateOutlet } from "./main-template-outlet";
import { Tests } from "pages/tests/tests";
import {
  AboutPageOutlet,
  CVPage,
  PortfolioPage,
  SkillsPage,
} from "pages/about";

export const Router = memo(() => (
  <Routes>
    <Route element={<AuthOutlet />}>
      <Route
        path={ROUTES.AUTH}
        element={<AuthPage ROUTES={ROUTES} setAuthorized={setAuthorized} />}
      />
      <Route path="/" element={<MainTemplateOutlet />}>
        <Route path={ROUTES.TESTS.INDEX}>
          <Route index element={<Tests />} />
        </Route>
        <Route path={ROUTES.ABOUT.INDEX} element={<AboutPageOutlet />}>
          <Route index element={<CVPage />} />
          <Route path={ROUTES.ABOUT.CV} element={<CVPage />} />
          <Route path={ROUTES.ABOUT.PORTFOLIO} element={<PortfolioPage />} />
          <Route path={ROUTES.ABOUT.SKILLS} element={<SkillsPage />} />
        </Route>
        <Route path="*" element={<AboutPageOutlet />}>
          <Route index element={<CVPage />} />
        </Route>
      </Route>
    </Route>
  </Routes>
));
