import { Outlet } from "react-router-dom";
import { MainTemplate, Sidebar } from "shared_ui/remote";
import { SIDEBAR } from "shared/config";
import { useCreateSidebarDataFromConfig } from "shared/lib";
import type { MouseEvent } from "react";
import { useTranslation } from "react-i18next";

export const MainTemplateOutlet = () => {
  const { i18n } = useTranslation();

  const sidebarData = useCreateSidebarDataFromConfig(SIDEBAR);

  const handleLanguageChange = (e: MouseEvent<HTMLElement>, lang: string) => {
    console.log("lang", lang);
    i18n.changeLanguage(lang);
  };

  console.log("sidebarData", sidebarData);
  return (
    <MainTemplate
      label="makarenkoff.ru"
      Sidebar={Sidebar}
      sidebarData={sidebarData}
      onLangToggleChange={handleLanguageChange}
    >
      <Outlet />
    </MainTemplate>
  );
};
