import type { ComponentType } from "react";
import { Provider } from "react-redux";

// (FSD-линтер-бета): относительно правильная ошибка, пока линтером не предусматривается использование redux
// eslint-disable-next-line boundaries/element-types
import { reduxStore } from "../store";

export const withRedux = (Component: ComponentType) => () =>
  (
    <Provider store={reduxStore}>
      <Component />
    </Provider>
  );
