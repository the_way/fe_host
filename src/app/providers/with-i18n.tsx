import type { ComponentType } from "react";
import { I18nextProvider } from "react-i18next";

import { i18n } from "shared/lib/i18n";

export const withI18n = (Component: ComponentType) => () =>
  (
    <I18nextProvider i18n={i18n}>
      <Component />
    </I18nextProvider>
  );
