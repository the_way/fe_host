import { compose } from "@reduxjs/toolkit";
import type { FunctionComponent } from "react";
import { withI18n } from "./with-i18n";

import { withRedux } from "./with-redux";
import { withRouter } from "./with-router";
import { withTheme } from "./with-theme";

export const withProviders = compose<FunctionComponent>(
  withI18n,
  withTheme,
  withRedux,
  withRouter
);
