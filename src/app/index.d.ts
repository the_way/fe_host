import type { rootReducer } from "./store/root-reducer";

declare global {
  type GlobalStore = ReturnType<typeof rootReducer>;
}
