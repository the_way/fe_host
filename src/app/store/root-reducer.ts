import { combineReducers } from "@reduxjs/toolkit";
import { baseApi, authReducer } from "auth/remote";
import { feedReducer } from "entities/feed/model/feed-slice";

export const rootReducer = combineReducers({
  feed: feedReducer,
  auth: authReducer,
  [baseApi.reducerPath]: baseApi.reducer,
});

export type GlobalStore = ReturnType<typeof rootReducer>;
