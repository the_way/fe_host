import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { withProviders } from "./providers";
import { Router } from "./router/router";
import { store } from "./store/store";

export const App = withProviders(() => <Router />);
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("app")
);
