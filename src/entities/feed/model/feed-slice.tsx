// // TODO: (AM) SWITCH ON CHECKING!  _!  _!  _!  _!  _!  _!  _!  _!  _!  _!  _!  _!
// //  @ts-nocheck
// /* eslint-disable */
/* eslint-disable @typescript-eslint/unbound-method */
import { createEntityAdapter, createSlice } from "@reduxjs/toolkit";

export const feedAdapter = createEntityAdapter();

// type TInititalState = {
//   categories?: {
//     sport: "sport";
//   };
//   news?: {
//     [id: string]: {
//       title: string;
//       description: string;
//     };
//   };
//   feed: {};
// };

const initialState = feedAdapter.getInitialState();

const feedSlice = createSlice({
  name: "feed",
  initialState,

  reducers: {
    addNews: feedAdapter.addMany,
  },
});

export const { reducer: feedReducer, actions: feedActions } = feedSlice;
