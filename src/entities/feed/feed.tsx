import { useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { useDispatch } from "react-redux";
import { Post } from "shared_ui/remote";
import type { TFeed } from "shared_ui/remote";
import type { GlobalStore } from "src/app/store/root-reducer";
import { useGetAllQuery } from "shared/api/feed-api";
import { feedActions, feedAdapter } from "./model/feed-slice";

export const FeedEntity = () => {
  const [items, setItems] = useState<JSX.Element[] | null>(null);
  // const { selectEntities } = feedSelectors;
  // const selectedEntities = useSelector(selectEntities);
  const dispatch = useDispatch();

  const { data, isSuccess, isError, isLoading } = useGetAllQuery({});

  useEffect(() => {
    if (isSuccess) {
      dispatch(feedActions.addNews(data));
    }
  }, [isSuccess]);

  useEffect(() => {
    if (data) {
      setItems(dataMapping(data.slice(0, 10)));
    }
  }, [data]);

  if (isError) {
    return <div> Error</div>;
  }
  if (isLoading) {
    return <div>Loading</div>;
  }

  const dataMapping = (arr: TFeed[]) =>
    arr.map(({ title, shortDescription, fullDescription, category }) => (
      <Post
        title={title}
        shortDescription={shortDescription}
        fullDescription={fullDescription}
        category={category}
        key={title}
      />
    ));

  return (
    <InfiniteScroll
      dataLength={items?.length || 0}
      next={
        () =>
          setItems((prev) => [
            ...(prev || []),
            ...dataMapping(data.slice(prev?.length, (prev?.length || 0) + 10)),
          ])
        // false
      }
      hasMore
      loader={<h4>Loading...</h4>}
      endMessage={
        <p style={{ textAlign: "center" }}>
          <b>Yay! You have seen it all</b>
        </p>
      }
    >
      {items}
    </InfiniteScroll>
  );
};

export const feedSelectors = feedAdapter.getSelectors(
  (state: GlobalStore) => state.feed
);
