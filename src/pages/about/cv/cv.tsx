import { Divider } from "@mui/material";
import { useTranslation } from "react-i18next";
import { Link, BaseTypography, Body1 } from "shared_ui/remote";
import { StyledMltn, StyledMtshv } from "../styles";
import { Block } from "./block";
import { StyledQuoteBox } from "./styles";

export const CVPage = () => {
  const { t } = useTranslation("main", { keyPrefix: "pages.about.cv" });
  console.log(
    't("experience.vilmary.description")',
    t("experience.vilmary.description", { returnObjects: true })
  );
  return (
    <>
      <Block blockTitle={t("recommendation.title")} variant="text">
        <>
          <BaseTypography marginBottom="20px">
            {t("recommendation.mltnQuote")}
          </BaseTypography>
          <StyledQuoteBox>
            <StyledMltn />
            <BaseTypography variant="body2">
              <Link to="https://www.mltn.dev/" isExternal>
                {t("recommendation.mltnName")}
              </Link>
              , {t("recommendation.mltnCaption")}
            </BaseTypography>
          </StyledQuoteBox>

          <br />
          <Divider />
          <br />

          <BaseTypography marginBottom="20px">
            {t("recommendation.mtsvQuote")}
          </BaseTypography>
          <StyledQuoteBox>
            <StyledMtshv />
            <BaseTypography variant="body2">
              <Link to="https://t.me/thevitali" isExternal>
                {t("recommendation.mtsvName")}
              </Link>
              , {t("recommendation.mtsvCaption")}
            </BaseTypography>
          </StyledQuoteBox>
        </>
      </Block>

      <Block
        blockTitle={t("experience.title")}
        variant="list"
        content={{
          title: "Vilmari, Moscow ",
          subtitle: t("experience.vilmary.subtitle"),
        }}
        stack={[
          "React",
          "TypeScript",
          "Redux TK Query",
          "Feature-Sliced Design",
          "Mui",
          "React-Hook-Forms",
          "CSS-in-JS",
          "Storybook",
          "Jest + Testing Library",
        ]}
      >
        {t("experience.vilmary.description", { returnObjects: true })}
      </Block>
      <Block
        blockTitle=""
        variant="list"
        content={{
          title: "Metalamp, Tomsk",
          subtitle: t("experience.metalamp.subtitle"),
        }}
        stack={[
          "JS",
          "TypeScript",
          "React",
          "BEM",
          "Webpack",
          "JQuery",
          "Pug",
          "Sass",
        ]}
      >
        {t("experience.metalamp.description", { returnObjects: true })}
      </Block>
      <Block blockTitle={t("aboutMe.title")} variant="text">
        <>
          <Body1>{t("aboutMe.main")}</Body1>
          {t("aboutMe.boldThesises", { returnObjects: true }).map((text) => (
            <Body1 textAlign="center" marginTop={2}>
              <b>{text}</b>
            </Body1>
          ))}
        </>
      </Block>
      <Block
        blockTitle={t("education.title")}
        variant="list"
        content={{
          title: t("education.name"),
          subtitle: "2008-2013",
        }}
      >
        {[`${t("education.speciality")}`]}
      </Block>
      <Block blockTitle={t("languages")} variant="list">
        {["English (B2)"]}
      </Block>
    </>
  );
};
