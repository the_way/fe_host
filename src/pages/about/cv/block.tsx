import { List } from "@mui/material";
import type { ReactElement } from "react";
import { BaseTypography, Chip } from "shared_ui/remote";
import { StyledCard } from "../styles";
import { ListItem } from "./list-item";
import { StyledBlockContainer } from "./styles";

type TContentCommon = {
  title: string;
  subtitle: string;
};

type TVariant = "list" | "text";

type TBlockProps<T extends TVariant> = {
  blockTitle: string;
  content?: TContentCommon;
  children: T extends "list" ? string[] : ReactElement;
  variant: T;
  stack?: string[];
};

export const Block = <T extends TVariant>(
  props: T extends "list" ? TBlockProps<"list"> : TBlockProps<"text">
) => {
  const { blockTitle, children, content, variant, stack } = props;

  return (
    <StyledBlockContainer>
      <BaseTypography variant="h3">{blockTitle}</BaseTypography>
      <StyledCard>
        <BaseTypography variant="subtitle1">{content?.title}</BaseTypography>
        <BaseTypography variant="body1" fontStyle="italic">
          {content?.subtitle}
        </BaseTypography>
        {stack &&
          stack.map((label) => <Chip label={label} size="small" key={label} />)}
        {variant === "text" ? (
          <>{children}</>
        ) : (
          <List>
            {children.map((value) => (
              <ListItem key={value}>{value}</ListItem>
            ))}
          </List>
        )}
      </StyledCard>
    </StyledBlockContainer>
  );
};
