import DoneIcon from "@mui/icons-material/Done";
import type { ReactElement } from "react";
import { BaseTypography } from "shared_ui/remote";
import { StyledListItem } from "../styles";

type TListItemProps = {
  children: ReactElement | string;
};

export const ListItem = ({ children }: TListItemProps) => (
  <StyledListItem>
    <DoneIcon fontSize="small" />
    <BaseTypography variant="body1">{children};</BaseTypography>
  </StyledListItem>
);
