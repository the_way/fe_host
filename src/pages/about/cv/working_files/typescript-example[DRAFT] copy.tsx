import { List } from "@mui/material";
import { Typography } from "shared_ui/remote";
import { StyledCard } from "../../styles";
import { ListItem } from "../list-item";

type TContentCommon = {
  title: string;
  subtitle: string;
};

type TVariant = "list" | "text";
type TBlockProps<T extends TVariant> = {
  blockTitle: string;
  content?: TContentCommon;
  children: T extends "list" ? string[] : string;
  variant: T;
};

export const Block = <T extends TVariant>(
  props: T extends "list" ? TBlockProps<"list"> : TBlockProps<"text">
) => {
  const { blockTitle, children, content, variant } = props;

  return (
    <section>
      <Typography variant="h3">{blockTitle}</Typography>
      <StyledCard>
        <Typography fontFamily="fantasy" variant="subtitle1">
          {content?.title}
        </Typography>
        <Typography fontFamily="fantasy" variant="body1" fontStyle="italic">
          {content?.subtitle}
        </Typography>
        {variant === "text" ? (
          <>{children}</>
        ) : (
          <List>
            {children.map((value) => (
              <ListItem>{value}</ListItem>
            ))}
          </List>
        )}
      </StyledCard>
    </section>
  );
};
