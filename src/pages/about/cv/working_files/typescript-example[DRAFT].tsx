import { List } from "@mui/material";
import { Typography } from "shared_ui/remote";
import { StyledCard } from "../../styles";
import { ListItem } from "../list-item";

type TContentCommon = {
  title: string;
  subtitle: string;
};

type TBlockProps1 = {
  blockTitle: string;
  content?: TContentCommon;
  children: string[];
  variant: "list";
};
type TBlockProps2 = {
  blockTitle: string;
  content?: TContentCommon;
  children: string;
  variant: "text";
};
// } & (TTextVariant || TListVariant);

export const Block = <T extends TBlockProps1 | TBlockProps2>(props: T) => {
  const { blockTitle, children, content, variant } = props;
  if (variant === "list") {
    children;
  }
  return (
    <section>
      <Typography variant="h3">{blockTitle}</Typography>
      <StyledCard>
        <Typography fontFamily="fantasy" variant="subtitle1">
          {content?.title}
        </Typography>
        <Typography fontFamily="fantasy" variant="body1" fontStyle="italic">
          {content?.subtitle}
        </Typography>
        {variant === "text" ? (
          <>{children}</>
        ) : (
          <List>
            {children.map((value) => (
              <ListItem>{value}</ListItem>
            ))}
          </List>
        )}
      </StyledCard>
    </section>
  );
};
