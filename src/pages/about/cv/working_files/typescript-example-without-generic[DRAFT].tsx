import { List } from "@mui/material";
import type { ReactElement } from "react";
import { Typography } from "shared_ui/remote";
import { StyledCard } from "../../styles";
import { ListItem } from "../list-item";

type TContentCommon = {
  title: string;
  subtitle: string;
};

type TListVariant = { variant: "list" };
type TTextVariant = { variant: "text" };

type TBlockProps =
  | ({
      blockTitle: string;
      content?: TContentCommon;
      children: string | ReactElement;
    } & TTextVariant)
  | ({
      blockTitle: string;
      content?: TContentCommon;
      children: string[];
    } & TListVariant);

export const Block = ({
  blockTitle,
  children,
  content,
  variant,
}: TBlockProps) => {
  if (variant === "list") {
    children;
  }
  return (
    <section>
      <Typography variant="h3">{blockTitle}</Typography>
      <StyledCard>
        <Typography variant="subtitle1">{content?.title}</Typography>
        <Typography variant="body1" fontStyle="italic">
          {content?.subtitle}
        </Typography>
        {variant === "text" ? (
          <>{children}</>
        ) : (
          <List>
            {children.map((value) => (
              <ListItem>{value}</ListItem>
            ))}
          </List>
        )}
      </StyledCard>
    </section>
  );
};
