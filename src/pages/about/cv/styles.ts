import { styled } from "@mui/material";

export const StyledQuoteBox = styled("div")(({ theme }) => ({
  display: "grid",
  gridTemplateColumns: "max-content 1fr",
  gridGap: theme.spacing(3),
  alignItems: "center",
}));

export const StyledBlockContainer = styled("section")(() => ({
  width: "97%",
  marginTop: "50px",
}));
