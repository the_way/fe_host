import { Outlet } from "react-router-dom";
import { CommonTopBlock } from "./common-top-block";

export const AboutPageOutlet = () => {
  return (
    <div>
      <CommonTopBlock />
      <Outlet />
    </div>
  );
};
