import { H1, H3, Subtitle2 } from "shared_ui/remote";

import { StyledCard } from "../styles";
import {
  JuniorSkillsBox,
  MiddleSkillsBox,
  SeniorSkillsBox,
  AlgorithmsSkillBox,
  TheorySkillBox,
} from "./skill-boxes";

export const RoadMap = () => (
  <StyledCard>
    <H1 textAlign="center">Roadmap to Senior-level</H1>

    <div style={{ marginBottom: "48px" }}>
      <Subtitle2>
        <b>Целью</b> данного блока является отслеживание получения навыков,
        соответствующих условным среднерыночным уровням middle / senior.
      </Subtitle2>
      <Subtitle2>
        Ниже приведён список необходимых навыков для каждого грейда ( middle /
        senior) и ссылки на работы, подтверждающие наличие этих навыков.{" "}
        <i>(в процессе заполнения)</i>
      </Subtitle2>
    </div>
    <H3 textAlign="center">Теория</H3>
    <TheorySkillBox />
    <H3 textAlign="center">Алгоритмы</H3>
    <AlgorithmsSkillBox />
    <H3 textAlign="center">Практика</H3>
    <JuniorSkillsBox />
    <MiddleSkillsBox />
    <SeniorSkillsBox />
  </StyledCard>
);
