import { H4 } from "shared_ui/remote";
import { ListItemWithLink } from "../../components";
import { StyledList } from "./styles";

export const SeniorSkillsBox = () => (
  <>
    <H4>Навыки senior frontend-разработчика:</H4>
    <StyledList>
      <ListItemWithLink
        links={[
          {
            to: "https://gitlab.com/the_way",
            linkTitle: "Проект 'makarenkoff.ru' на гитлабе",
            isExternal: true,
          },
        ]}
      >
        Опыт реализация проектов "с нуля до деплоя")
      </ListItemWithLink>
      <ListItemWithLink
        links={[
          {
            to: "https://gitlab.com/the_way",
            linkTitle: "Проект 'makarenkoff.ru' на гитлабе",
            isExternal: true,
          },
        ]}
      >
        Микросервисная архитектура (WMF)
      </ListItemWithLink>
      <ListItemWithLink
        links={[
          {
            to: "https://gitlab.com/the_way/",
            linkTitle: "gitlab.yml файл в каждом микросервисе проекта ",
            isExternal: true,
          },
        ]}
      >
        Полностью автоматизированный CI/CD gitlab
      </ListItemWithLink>
      <ListItemWithLink
        links={[
          {
            to: "https://gitlab.com/the_way/",
            linkTitle: "Dockerfile файл в каждом микросервисе проекта ",
            isExternal: true,
          },
          {
            to: "https://gitlab.com/the_way/root",
            linkTitle: "docker-compose.yml + nginx.conf ",
            isExternal: true,
          },
        ]}
      >
        Деплой (docker, docker-compose, nginx)
      </ListItemWithLink>
      <ListItemWithLink
        links={[
          {
            to: "https://gitlab.com/the_way/fe_host/-/tree/master/webpack",
            linkTitle:
              "Конфигурация webpack на микросервисе FE_HOST, проект 'makarenkoff.ru' на гитлабе ",
            isExternal: true,
          },
        ]}
      >
        Продвинутая настройка webpack (module federation, минификация,
        merge-конфигов, сжатие)
      </ListItemWithLink>
      <ListItemWithLink
        links={[
          {
            to: "https://gitlab.com/the_way/be_main",
            linkTitle:
              "проект 'makarenkoff.ru' на гитлабе: реализация простого бэка на express, jwt",
            isExternal: true,
          },
          {
            to: "http://makarenkoff.ru/authentication",
            linkTitle: "makarenkoff.ru: пример рыботы jwt",
            isExternal: true,
          },
        ]}
      >
        Понимание принципов работы node.js и бэк'енда, понимание JWT,
        реляционных СУБД
      </ListItemWithLink>

      <ListItemWithLink
        links={[
          {
            to: "https://github.com/makarenkovdo/range-slider",
            linkTitle:
              "Демонстрационная страница, список зависимостей, описание API, описание и визуализация архитектуры через UML-диаграммы, стажировочное задание в metalamp, 11.2021",
            isExternal: true,
          },
          {
            to: "https://dopaminer.notion.site/b4477b7654284d668dc7927e9466d95e",
            linkTitle:
              "makarenkoff.ru: процессы, code style, архитектура проекта",
            isExternal: true,
          },
        ]}
      >
        Документирование проекта
      </ListItemWithLink>
      <ListItemWithLink
        links={[
          {
            to: "https://github.com/makarenkovdo/range-slider",
            linkTitle:
              "Демонстрационная страница, список зависимостей, описание API, описание и визуализация архитектуры через UML-диаграммы, стажировочное задание в metalamp, 11.2021",
            isExternal: true,
          },
          {
            to: "https://dopaminer.notion.site/SCRUMBAN-100f1006355f44cbb8634e815d489474",
            linkTitle: "Применение scrumban при написании makarenkoff.ru",
            isExternal: true,
          },
        ]}
      >
        Применение agile-методологии (scrumban)
      </ListItemWithLink>
      <ListItemWithLink
        links={[
          {
            to: "https://app.weeek.net/ws/286047/shared/board/GaNwhHGtwZRHwlJUXrXpZAUTl8Dnygqn",
            linkTitle: "В списке задач (спринт, октябрь 2022)",
            isExternal: true,
          },
        ]}
      >
        Реализация SSR (Next.js)
      </ListItemWithLink>
      <ListItemWithLink
        links={[
          {
            to: "https://app.weeek.net/ws/286047/shared/board/GaNwhHGtwZRHwlJUXrXpZAUTl8Dnygqn",
            linkTitle: "В списке задач (спринт, октябрь 2022)",
            isExternal: true,
          },
        ]}
      >
        Реализация взаимодействия через Graphql API (+apollo client)
      </ListItemWithLink>
    </StyledList>
  </>
);
