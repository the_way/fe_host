import { H4 } from "shared_ui/remote";
import { ListItemWithLink } from "../../components";
import { StyledList } from "./styles";

export const AlgorithmsSkillBox = () => (
  <StyledList disablePadding>
    <ListItemWithLink
      links={[
        {
          to: "https://dopaminer.notion.site/Roadmap-algorithms-2435714661e34b92b700b5675f5aadb9",
          linkTitle: "ссылка на подборку решений в notion",
          isExternal: true,
        },
      ]}
    >
      <>Решения задач с leetcode с комментариями</>
    </ListItemWithLink>
  </StyledList>
);
