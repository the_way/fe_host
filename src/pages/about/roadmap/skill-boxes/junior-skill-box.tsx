import { H4 } from "shared_ui/remote";
import { ListItemWithLink } from "../../components";
import { StyledList } from "./styles";

export const JuniorSkillsBox = () => (
  <>
    <H4>Навыки junior frontend-разработчика:</H4>
    <StyledList disablePadding>
      <ListItemWithLink
        links={[
          {
            to: "https://github.com/makarenkovdo/metalamp-task2",
            linkTitle:
              "Компонентная вёрстка сайта отеля, стажировочное задание в metalamp, 11.2021",
            isExternal: true,
          },
        ]}
      >
        <>
          html, sass, вёрстка по макету figma{" "}
          <i>
            (компонентная вёрсткая (pug), отзывчивая вёрстка (css grid + media
            query))
          </i>
          , применение BEM, базовый js, jquery
        </>
      </ListItemWithLink>
    </StyledList>
  </>
);
