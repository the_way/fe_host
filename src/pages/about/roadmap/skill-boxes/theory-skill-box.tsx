import { ListItemWithLink } from "../../components";
import { StyledList } from "./styles";

export const TheorySkillBox = () => (
  <StyledList disablePadding>
    <ListItemWithLink
      links={[
        {
          to: "https://dopaminer.notion.site/Roadmap-theory-4c01e98a6e9c478c9917f1a49fbcb548",
          linkTitle: "ссылка на теоретические конспекты (notion)",
          isExternal: true,
        },
      ]}
    >
      <>
        Теория middle / senior
        <i>
          (js, браузер, сеть, архитектура, ооп, фп, системы сборки, оптимизации,
          типизация и т.д.)
        </i>
      </>
    </ListItemWithLink>
    <ListItemWithLink
      links={[
        {
          to: "https://dopaminer.notion.site/Book-summaries-7bd039f0b0154aa7a6a41c079ffdf539",
          linkTitle: "ссылка на book summaries (notion)",
          isExternal: true,
        },
      ]}
    >
      <>
        Конспекты книг по frontend'у и программированию в целом. Уровень middle
        / senior
      </>
    </ListItemWithLink>
  </StyledList>
);
