import { List, styled } from "@mui/material";

export const StyledList = styled(List)(() => ({
  "& .MuiList-padding": {
    padding: "0px",
  },
}));
