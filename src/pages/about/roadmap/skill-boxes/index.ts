export { JuniorSkillsBox } from "./junior-skill-box";
export { MiddleSkillsBox } from "./middle-skills-box";
export { SeniorSkillsBox } from "./senior-skills-box";
export { AlgorithmsSkillBox } from "./algorithms-skill-box";
export { TheorySkillBox } from "./theory-skill-box";
