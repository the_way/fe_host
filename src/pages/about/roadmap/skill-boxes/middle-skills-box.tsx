import { H4 } from "shared_ui/remote";
import { ListItemWithLink } from "../../components";
import { StyledList } from "./styles";

export const MiddleSkillsBox = () => {
  if (45 === 45) {
    return <div>123</div>;
  }
  return (
    <>
      <H4>Навыки middle frontend-разработчика:</H4>

      <StyledList>
        <ListItemWithLink
          links={[
            {
              to: "https://github.com/makarenkovdo/range-slider",
              linkTitle:
                "Range-Slider плагин, стажировочное задание в metalamp, 11.2021",
              isExternal: true,
            },
          ]}
        >
          Продвинутый js, навыки проектирования (ООП, реализация MVP
          архитектуры, разделение ответственности), создание библиотек с удобным
          API, разделения конфигурирования и бизнес-логики, документирование
          свой продукт (описание архитектуры, визуализация через UML-диаграммы),
          unit-тестирования, паттерны проектирования (observer, facade)
        </ListItemWithLink>
        <ListItemWithLink
          links={[
            {
              to: "https://github.com/makarenkovdo/oop-game",
              linkTitle:
                "ООП-аркада, стажировочное задание в metalamp, 11.2021",
              isExternal: true,
            },
          ]}
        >
          Паттерны проектирования (factory, abstract factory, builder, bridge,
          mediator, decorator, proxy, strategy, memento, )
        </ListItemWithLink>
        <ListItemWithLink
          links={[
            {
              to: "https://github.com/makarenkovdo/metalamp-task2/blob/master/webpack.config.js",
              linkTitle:
                "webpack.config.js к стажировочному заданию по вёрстке в metalamp, 11.2021",
              isExternal: true,
            },
            {
              to: "https://github.com/makarenkovdo/range-slider",
              linkTitle:
                "webpack.config.js (common/dev/prod) к стажировочному заданию по js в metalamp, 11.2021",
              isExternal: true,
            },
          ]}
        >
          Базовая настройка webpack (2 ссылки на стажировку)
        </ListItemWithLink>
        <ListItemWithLink
          links={[
            {
              to: "https://gitlab.com/the_way/fe_auth/-/tree/master/src/shared/api",
              linkTitle: "makarenkoff.ru, gitlab, fe_shared_ui/shared/api",
              isExternal: true,
            },
          ]}
        >
          Продвинутая настройка RTK Query (fetchBaseQuery + injected endpoints)
        </ListItemWithLink>
        <ListItemWithLink
          links={[
            {
              to: "https://gitlab.com/the_way/fe_host/-/tree/master/src/app/providers",
              linkTitle:
                "Композиция провайдеров с помощью HOC'ов, makarenkoff.ru, gitlab,",
              isExternal: true,
            },
            {
              to: "https://app.weeek.net/ws/286047/shared/board/OOIftVyMvMS71bYXxEwoRcKTaLje4goP",
              linkTitle: "остальные задачи в спринте 08/22",
              isExternal: true,
            },
          ]}
        >
          Продвинутый React: композиция компонентов в больших модулях, HOC’s,
          контроль ререндеров/перерисовки, lazy-loading,
        </ListItemWithLink>
        <ListItemWithLink
          links={[
            {
              to: "https://gitlab.com/the_way/fe_shared_ui/-/tree/master/src/shared/lib/story-factory",
              linkTitle:
                "generic-types для получения проверки и автокомплита при применении 'story-фабрик'",
              isExternal: true,
            },
            {
              to: "https://app.weeek.net/ws/286047/shared/board/OOIftVyMvMS71bYXxEwoRcKTaLje4goP",
              linkTitle:
                "задача в спринте 08.2022 (dynamic-routes): generic-types, recoursive types, mapped types, conditional types",
              isExternal: true,
            },
          ]}
        >
          Продвинутый typescript
        </ListItemWithLink>
        <ListItemWithLink
          links={[
            {
              to: "https://github.com/makarenkovdo/range-slider/tree/master/test",
              linkTitle:
                "Unit-тесты на jest+jsdom+testing library к стажировочному задание в metalamp, 11.2021",
              isExternal: true,
            },
            {
              to: "https://gitlab.com/the_way/fe_shared_ui/-/blob/master/src/shared/ui/atoms/button/button.stories.tsx",
              linkTitle:
                "makarenkoff.ru, gitlab: пример написания story-теста для компонента button",
              isExternal: true,
            },
            {
              to: "https://app.weeek.net/ws/286047/shared/board/V5rfcyE8NdYFjIm8E1mcAUPBfYg0U44C",
              linkTitle: "задачи по расширенному тестированию в спринте 09.22",
              isExternal: true,
            },
          ]}
        >
          Написание unit-тестов, UI-тестов
        </ListItemWithLink>
      </StyledList>
    </>
  );
};
