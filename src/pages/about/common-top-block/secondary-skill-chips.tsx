import { useMemo } from "react";
import { Chip, Grid } from "shared_ui/remote";

export const SecondarySkillChips = () => {
  const secondaryChipsGridBreakpointsStyles = useMemo(
    () => ({
      xs: {
        gridTemplateColumns: "repeat(auto-fill, minmax(60px, 100px))",
        justifyContent: "center",
      },
    }),
    []
  );

  const skills = useMemo(
    () => [
      "Redux TKQ",
      "React Query",
      "FSD",
      "Webpack",
      "Mui",
      "CSS-in-JS",
      "CI/CD",
      "Docker",
      "Antd",
      "Hook-form",
      "Graphql",
      "Linux",
      "Effector",
      "Zustand",
      "Node.js",
    ],
    []
  );

  return (
    <Grid width="100%" {...secondaryChipsGridBreakpointsStyles}>
      {skills.map((label) => (
        <Chip label={label} variant="outlined" size="small" key={label} />
      ))}
    </Grid>
  );
};
