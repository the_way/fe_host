import { Box } from "@mui/material";
import { useMemo } from "react";
import { useTranslation } from "react-i18next";
import { TelegramIcon, BaseTypography, Grid } from "shared_ui/remote";
import { StyledCommonTopBlockCard, StyledPhoto } from "../styles";
import { MainSkillChips } from "./main-skill-chips";
import { SecondarySkillChips } from "./secondary-skill-chips";

export const CommonTopBlock = () => {
  const { t } = useTranslation("main", { keyPrefix: "pages.about.cv" });
  const mainGridBreakpointsStyles = useMemo(
    () => ({
      sm: { width: "80%" },
      md: { width: "50%" },
    }),
    []
  );

  return (
    <Box width={"97%"}>
      <StyledCommonTopBlockCard>
        <Grid
          display="grid"
          justifyItems="center"
          {...mainGridBreakpointsStyles}
        >
          <StyledPhoto />
          <BaseTypography variant="h3">{t("name")}</BaseTypography>
          <BaseTypography variant="h6">Frontend Developer</BaseTypography>
          <a href="https://t.me/thedropofit">
            <TelegramIcon />
          </a>
          <MainSkillChips />
          <SecondarySkillChips />
        </Grid>
      </StyledCommonTopBlockCard>
    </Box>
  );
};
