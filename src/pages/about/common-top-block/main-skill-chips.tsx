import { Box } from "@mui/material";
import { useMemo } from "react";
import { Chip } from "shared_ui/remote";

export const MainSkillChips = () => {
  const mainSkills = useMemo(() => ["JS", "React", "TypeScript"], []);
  return (
    <Box display="grid" gridAutoFlow="column">
      {mainSkills.map((label) => (
        <Chip label={label} variant="outlined" size="large" key={label} />
      ))}
    </Box>
  );
};
