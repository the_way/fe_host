export { AboutPageOutlet } from "./about-page-outlet";
export { CVPage } from "./cv";
export { PortfolioPage } from "./portfolio";
export { SkillsPage } from "./skills";
