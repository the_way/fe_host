import { List } from "@mui/material";
import { useTranslation } from "react-i18next";
import { H6 } from "shared_ui/remote";

import { ListItem } from "../../components";
import { StyledListSubItem } from "../../components/styles";
import { StyledCard } from "../../styles";

export const Description = () => {
  const { t } = useTranslation("main", {
    keyPrefix: "pages.about.portfolio.makarenkovProject.description",
  });
  console.log(
    't("fullstackSubtitleList", { returnObjects: true })',
    t("fullstackSubtitleList", { returnObjects: true })
  );
  return (
    <StyledCard>
      <H6>{t("descriptionTitle")}</H6>
      <List>
        <ListItem icon="success">{t("fullstackTitle")}</ListItem>
        {t("fullstackSubtitleList", { returnObjects: true }).map((text) => (
          <StyledListSubItem icon="success">{text}</StyledListSubItem>
        ))}
        {t("list", { returnObjects: true }).map((text) => (
          <ListItem icon="success">{text}</ListItem>
        ))}
      </List>
    </StyledCard>
  );
};
