import { useTranslation } from "react-i18next";
import { Subtitle1, Subtitle2 } from "shared_ui/remote";

import { StyledCard } from "../../styles";
import { CodeLinks } from "./code-links";
import { Description } from "./description";

export const MakarenkoffProject = () => {
  const { t } = useTranslation("main", {
    keyPrefix: "pages.about.portfolio.makarenkovProject",
  });
  return (
    <StyledCard>
      <Subtitle1 textAlign="center">{t("title")}</Subtitle1>
      <Subtitle2>{t("aim")}</Subtitle2>
      <Description />
      <CodeLinks />
    </StyledCard>
  );
};
