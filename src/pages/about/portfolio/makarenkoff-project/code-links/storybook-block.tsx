import { Body1, Link } from "shared_ui/remote";

import { ListItem } from "../../../components";

export const StorybookBlock = () => {
  return (
    <ListItem>
      <>
        <Body1>Storybook</Body1>
        <ListItem icon="dash">
          <Link
            isExternal
            to="https://gitlab.com/the_way/fe_shared_ui/-/tree/master/src/shared/lib/story-factory"
          >
            Фабрика по производству stories (HOCS, generics types, mapped types)
          </Link>
        </ListItem>
        <ListItem icon="dash">
          <Link
            isExternal
            to="https://gitlab.com/the_way/fe_shared_ui/-/blob/master/src/shared/ui/atoms/button/button.stories.tsx"
          >
            Пример stories компонента button
          </Link>
        </ListItem>
      </>
    </ListItem>
  );
};
