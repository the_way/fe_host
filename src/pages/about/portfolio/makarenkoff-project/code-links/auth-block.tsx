import { Body1, Link } from "shared_ui/remote";

import { ListItem } from "../../../components";

export const AuthBlock = () => {
  return (
    <ListItem>
      <>
        <Body1>Аутентификация</Body1>
        <ListItem icon="dash">
          <Link
            isExternal
            to="https://gitlab.com/the_way/fe_auth/-/blob/master/src/shared/api/base/base-query-with-token.ts"
          >
            Базовый запрос с токеном (redux tkq)
          </Link>
        </ListItem>
        <ListItem icon="dash">
          <Link
            isExternal
            to="https://gitlab.com/the_way/fe_auth/-/blob/master/src/app/router/auth-outlet.tsx"
          >
            Auth-outlet для react-router-6 (обёртка-маршрутизатор)
          </Link>
        </ListItem>
        <ListItem icon="dash">
          <Link
            isExternal
            to="https://gitlab.com/the_way/fe_auth/-/tree/master/src/features/auth"
          >
            Auth-feature (redux slice, кастомный хук для RTKQ)
          </Link>
        </ListItem>
        <ListItem icon="dash">
          <Link
            isExternal
            to="https://gitlab.com/the_way/fe_auth/-/blob/master/src/widgets/auth/auth-widget.tsx"
          >
            Auth-widget (Контроллеры-обётки (react-hook-form + mui) из shared_ui
            сервиса)
          </Link>
        </ListItem>
      </>
    </ListItem>
  );
};
