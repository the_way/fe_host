import { List } from "@mui/material";
import { H6, Link } from "shared_ui/remote";

import { ListItem } from "../../../components";
import { StyledCard } from "../../../styles";
import { AuthBlock } from "./auth-block";
import { DockerBlock } from "./docker-block";
import { RouteBlock } from "./route-block";
import { SidebarBlock } from "./sidebar-block";
import { StorybookBlock } from "./storybook-block";

export const CodeLinks = () => {
  return (
    <StyledCard>
      <H6>Ссылки на наиболее важные части кода:</H6>
      <List>
        <ListItem icon="dash">
          <Link to="https://gitlab.com/the_way/" isExternal>
            gitlab-репозиторий
          </Link>
        </ListItem>
        <ListItem icon="dash">
          <Link
            isExternal
            to="https://gitlab.com/the_way/fe_host/-/tree/master/webpack"
          >
            webpack-конфигурация (host-сервис)
          </Link>
        </ListItem>
        <ListItem icon="dash">
          <Link
            isExternal
            to="https://gitlab.com/the_way/fe_host/-/blob/master/.gitlab-ci.yml"
          >
            CI/CD-конфигурация (host-сервис)
          </Link>
        </ListItem>
        <AuthBlock />
        <RouteBlock />
        <SidebarBlock />
        <DockerBlock />
        <StorybookBlock />
      </List>
    </StyledCard>
  );
};
