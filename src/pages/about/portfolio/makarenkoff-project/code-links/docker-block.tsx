import { Body1, Link } from "shared_ui/remote";

import { ListItem } from "../../../components";

export const DockerBlock = () => {
  return (
    <ListItem>
      <>
        <Body1>Docker + Docker-compose</Body1>
        <ListItem icon="dash">
          <Link
            isExternal
            to="https://gitlab.com/the_way/root/-/blob/master/docker-compose.yml"
          >
            docker-compose root-сервиса (общий конфиг)
          </Link>
        </ListItem>
        <ListItem icon="dash">
          <Link
            isExternal
            to="https://gitlab.com/the_way/fe_host/-/blob/master/Dockerfile"
          >
            Dockerfile (host-сервис)
          </Link>
        </ListItem>
      </>
    </ListItem>
  );
};
