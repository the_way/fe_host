import { Body1, Link } from "shared_ui/remote";

import { ListItem } from "../../../components";

export const RouteBlock = () => {
  return (
    <ListItem>
      <>
        <Body1>Роутинг</Body1>
        <ListItem icon="dash">
          <Link
            isExternal
            to="https://gitlab.com/the_way/fe_host/-/tree/master/src/app/router"
          >
            router + main outlet
          </Link>
        </ListItem>
        <ListItem icon="dash">
          <Link
            isExternal
            to="https://gitlab.com/the_way/fe_host/-/blob/master/src/shared/config/routes.ts"
          >
            routes-config
          </Link>
        </ListItem>
        <ListItem icon="dash">
          <Link
            isExternal
            to="https://gitlab.com/the_way/fe_host/-/blob/master/src/shared/lib/get-url/get-url.ts
            "
          >
            генератор links-config на основе routes-config (generics, mapped,
            conditional, recoursive types)
          </Link>
        </ListItem>
        <ListItem icon="dash">
          <Link
            isExternal
            to="https://gitlab.com/the_way/fe_host/-/blob/master/src/shared/config/links.ts"
          >
            links-config
          </Link>
        </ListItem>
      </>
    </ListItem>
  );
};
