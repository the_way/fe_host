import { Body1, Body2, Link } from "shared_ui/remote";

import { ListItem } from "../../../components";

export const SidebarBlock = () => {
  return (
    <ListItem>
      <>
        <Body1>Sidebar:</Body1>
        <Body2>
          stories описываются с помощью кастомных "фабрик" (HOC + продвинутый
          typescript (generics, mapped types))
        </Body2>
        <ListItem icon="dash">
          <Link
            isExternal
            to="https://gitlab.com/the_way/fe_shared_ui/-/tree/master/src/shared/ui/organisms/sidebar"
          >
            sidebar (UI-organism)
          </Link>
        </ListItem>
        <ListItem icon="dash">
          <Link
            isExternal
            to="https://gitlab.com/the_way/fe_host/-/blob/master/src/shared/config/sidebar/sidebar.tsx"
          >
            sidebar-config
          </Link>
        </ListItem>
        <ListItem icon="dash">
          <Link
            isExternal
            to="https://gitlab.com/the_way/fe_host/-/blob/master/src/shared/lib/use-create-sidebar-data-from-config.ts"
          >
            хук-трансформатор конфига для поддержки переводов)
          </Link>
        </ListItem>
      </>
    </ListItem>
  );
};
