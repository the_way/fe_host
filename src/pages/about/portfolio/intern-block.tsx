import { List } from "@mui/material";
import { H6, Link } from "shared_ui/remote";

import { ListItem } from "../components";
import { StyledCard } from "../styles";

export const InternBlock = () => {
  return (
    <StyledCard>
      <H6>Проекты со стажировки (август - ноябрь 2021):</H6>
      <List>
        <ListItem icon="dash">
          <Link to="https://github.com/makarenkovdo/metalamp-task2">
            Компонентная вёрстка сайта отеля: pug, sass, html, webpack, jquery,
            figma, pixel perfect
          </Link>
        </ListItem>
        <ListItem icon="dash">
          <Link to="https://github.com/makarenkovdo/range-slider">
            MVC слайдер, написанный на ООП подходе: js, typescript, webpack
            (+mediator, facade, observable design patterns);
          </Link>
        </ListItem>
        <ListItem icon="dash">
          <Link to="https://github.com/makarenkovdo/oop-game">
            ООП игра (с целью отработать паттерны проектирования: factory,
            abstract factory, builder, bridge, mediator, strategy, proxy,
            memento, decorator)
          </Link>
        </ListItem>
      </List>
    </StyledCard>
  );
};
