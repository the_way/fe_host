import { InternBlock } from "./intern-block";
import { MakarenkoffProject } from "./makarenkoff-project";

export const PortfolioPage = () => (
  <>
    <MakarenkoffProject />
    <InternBlock />
  </>
);
