import { Box, styled } from "@mui/material";
import type { ReactElement } from "react";
import { Body1, Body2 } from "shared_ui/remote";
import DoneIcon from "@mui/icons-material/Done";

type TProps = {
  children: ReactElement[];
};
export const StyledBox = styled(Box)(() => ({
  display: "grid",
  gridTemplateAreas: `
  "a b"
  "c d"
`,
  justifyContent: "center",
  alignItems: "end",
  columnGap: "10px",
}));

export const PortfolioItem = ({ children }: TProps) => {
  return (
    <>
      <StyledBox component="li">
        <DoneIcon />
        <Body1>{children[0]}</Body1>
        <Box marginLeft={2} gridArea="d">
          <Body2>{children[1]}</Body2>
        </Box>
      </StyledBox>
    </>
  );
};
