import { Card, ListItem, styled, Theme } from "@mui/material";
import type { CSSProperties } from "@mui/styled-engine";
import { Mltn, Mtshv, Photo } from "shared_ui/remote";

const commonStyles = (theme: Theme): CSSProperties => ({
  maxWidth: "1920px",
  [theme.breakpoints.down("sm")]: {
    padding: theme.spacing(1),
    margin: 0,
  },
  [theme.breakpoints.up("sm")]: {
    padding: theme.spacing(3),
    margin: theme.spacing(3),
    marginTop: theme.spacing(1),
  },
});

export const StyledCard = styled(Card)(({ theme }) => ({
  ...commonStyles(theme),
}));

export const StyledCommonTopBlockCard = styled(Card)(({ theme }) => ({
  display: "grid",
  justifyItems: "center",
  ...commonStyles(theme),
}));

export const StyledToggleCard = styled(Card)(({ theme }) => ({
  ...commonStyles(theme),
  display: "flex",
  justifyContent: "center",
}));

const commonPhotoStyles = (theme: Theme): CSSProperties => ({
  borderRadius: "50%",
  overflow: "hidden",
  objectFit: "cover",
});

export const StyledPhoto = styled(Photo)(({ theme }) => ({
  ...commonPhotoStyles(theme),
  width: "300px",
  height: "290px",
  marginBottom: theme.spacing(2),
}));

export const StyledMtshv = styled(Mtshv)(({ theme }) => ({
  ...commonPhotoStyles(theme),
  display: "flex",
  justifySelf: "end",
  width: "100px",
  height: "100px",
}));

export const StyledMltn = styled(Mltn)(({ theme }) => ({
  ...commonPhotoStyles(theme),
  width: "100px",
  height: "100px",
}));

export const StyledListItem = styled(ListItem)(() => ({
  display: "grid",
  gridTemplateColumns: "max-content 1fr",
  columnGap: "10px",
  // borderBottom: "1px dashed grey",
}));
