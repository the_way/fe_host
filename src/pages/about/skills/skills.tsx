import { useTranslation } from "react-i18next";
import { Grid, H5, SkillCard } from "shared_ui/remote";
import { StyledCard } from "../styles";
import { Legend } from "./legend";

export const SkillsPage = () => {
  const { t } = useTranslation("main", {
    keyPrefix: "pages.about.skills",
  });

  return (
    <Grid xs={{ width: "97vw" }} sm={{ width: "97%" }}>
      <StyledCard>
        <Legend />
        <div>
          <H5>{t("instruments")}</H5>
          <SkillCard skillName="JS" rating={3} />
          <SkillCard skillName="React" rating={3} />
          <SkillCard skillName="Typescript" rating={3} />
          <SkillCard skillName="Redux TKQ" rating={3} />
          <SkillCard skillName="HTML/CSS" rating={3} />
          <SkillCard skillName="Mui" rating={3} />
          <SkillCard skillName="Webpack" rating={3} />
          <SkillCard skillName="Storybook" rating={3} />
          <SkillCard skillName="React-hooh-forms" rating={3} />
          <SkillCard skillName="Css-in-JS" rating={3} />
          <SkillCard skillName="Jest + RTL" rating={2} />
          <SkillCard skillName="React Query" rating={2} />
          <SkillCard skillName="Docker" rating={2} />
          <SkillCard skillName="CI/CD" rating={2} />
          <SkillCard skillName="BEM" rating={2} />
          <SkillCard skillName="Sass" rating={2} />
          <SkillCard skillName="Final-forms" rating={2} />
          <SkillCard skillName="Nginx" rating={1} />
          <SkillCard skillName="Graphql" rating={1} />
          <SkillCard skillName="Next.js" rating={1} />
          <SkillCard skillName="Effector" rating={1} />
          <SkillCard skillName="Mobx" rating={1} />
          <SkillCard skillName="Zustand" rating={1} />
          <SkillCard skillName="Lodash" rating={1} />
          <SkillCard skillName="Ramda" rating={1} />
        </div>
        <div>
          <H5>{t("theory")}</H5>
          <SkillCard skillName="JS" rating={3} />
          <SkillCard skillName="Type systems" rating={3} />
          <SkillCard skillName="Bundling" rating={3} />
          <SkillCard skillName="Algorithms" rating={3} />
          <SkillCard skillName="Browser" rating={3} />
          <SkillCard skillName="Networks" rating={2} />
          <SkillCard skillName="Optimization" rating={3} />
          <SkillCard skillName="functional programming" rating={2} />
          <SkillCard skillName="OOP" rating={2} />
          <SkillCard skillName="Design Patterns" rating={3} />
          <SkillCard skillName="Architecture" rating={3} />
          <SkillCard skillName="Rendering" rating={1} />
          <SkillCard skillName="Deployment" rating={2} />
          <SkillCard skillName="Security" rating={1} />
        </div>
      </StyledCard>
    </Grid>
  );
};
