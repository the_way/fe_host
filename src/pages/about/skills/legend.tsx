import { useTranslation } from "react-i18next";
import { Card, SkillCard } from "shared_ui/remote";

export const Legend = () => {
  const { t } = useTranslation("main", {
    keyPrefix: "pages.about.skills.legend",
  });
  return (
    <Card
      collapseChildren={
        <>
          <SkillCard
            isOpenCollapse
            skillName="Proficiency"
            rating={4}
            shouldShowLabel={false}
            collapseData={[
              {
                title: t("proficiency"),
              },
            ]}
          />
          <SkillCard
            isOpenCollapse
            skillName="Advanced"
            rating={3}
            shouldShowLabel={false}
            collapseData={[
              {
                title: t("advanced"),
              },
            ]}
          />
          <SkillCard
            isOpenCollapse
            skillName="Intermediate"
            rating={2}
            shouldShowLabel={false}
            collapseData={[
              {
                title: t("intermediate"),
              },
            ]}
          />
          <SkillCard
            isOpenCollapse
            skillName="Elementary"
            rating={1}
            shouldShowLabel={false}
            collapseData={[
              {
                title: t("elementary"),
              },
            ]}
          />
          <SkillCard
            isOpenCollapse
            skillName="Zero"
            rating={0}
            shouldShowLabel={false}
            collapseData={[
              {
                title: t("zero"),
              },
            ]}
          />
        </>
      }
    >
      <>{t("legend")}</>
    </Card>
  );
};
