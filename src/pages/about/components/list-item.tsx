import DoneIcon from "@mui/icons-material/Done";
import RemoveIcon from "@mui/icons-material/Remove";
import type { ReactElement } from "react";
import { BaseTypography } from "shared_ui/remote";
import { StyledListItem } from "../styles";

type TListItemProps = {
  children: ReactElement | string;
  icon?: "dash" | "success";
};

export const ListItem = ({ children, icon, ...props }: TListItemProps) => (
  <StyledListItem {...props}>
    {icon === "success" && <DoneIcon />}
    {icon === "dash" && <RemoveIcon />}
    <BaseTypography variant="body1">{children}</BaseTypography>
  </StyledListItem>
);
