import { styled } from "@mui/material";
import { ListItem } from "./list-item";

export const StyledListSubItem = styled(ListItem)(({ theme }) => ({
  marginLeft: theme.spacing(4),
  fontSize: "8px",
}));
