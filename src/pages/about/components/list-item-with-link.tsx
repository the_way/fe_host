import DoneIcon from "@mui/icons-material/Done";
import RemoveIcon from "@mui/icons-material/Remove";
import { List } from "@mui/material";
import type { ReactElement } from "react";
import { BaseTypography, Link } from "shared_ui/remote";
import { StyledListItem } from "../styles";

type TListItemWithLinkProps = {
  children: ReactElement | string;
  icon?: "minus" | "success";
  links: {
    to: string;
    linkTitle: string;
    isExternal?: boolean;
  }[];
};

export const ListItemWithLink = ({
  children,
  icon = "success",
  links,
  ...props
}: TListItemWithLinkProps) => (
  <StyledListItem {...props}>
    {icon === "success" ? <DoneIcon /> : <RemoveIcon />}
    <BaseTypography variant="body1">{children}:</BaseTypography>
    <div />
    <div>
      <List>
        {links.map(({ linkTitle, ...rest }) => (
          <>
            <i>
              <Link fontSize={16} {...rest}>
                {linkTitle}
              </Link>
            </i>
            <br />
          </>
        ))}
      </List>
    </div>
  </StyledListItem>
);
