import { Box, List, ListItem } from "@mui/material";
import { Body1, Link } from "shared_ui/remote";
import { ROUTES } from "shared/config";

export const HomePage = () => (
  <Box>
    <Body1>
      На этом сайте будет отображаться результат работы с различными
      инструментами/библиотеками. Пока что - крайне "сыро", и единственный
      функционал (redux useQuery + infinity-scroll) <span>&nbsp;</span>
      <Link to={ROUTES.FEED} isExternal>
        здесь!
      </Link>
      <span>&nbsp;</span>Все нижеперечисленные задачи в соответсвующих
      спринтах...{" "}
      <Link
        to="https://app.weeek.net/ws/286047/shared/board/OOIftVyMvMS71bYXxEwoRcKTaLje4goP"
        isExternal
      >
        ссылка!
      </Link>{" "}
      Нужно время..! :)
    </Body1>
    <div>
      <List>
        <b>Redux TKQ:</b>
        <ListItem>entityAdapter</ListItem>
        <ListItem>extraReducers</ListItem>
        <ListItem>re-reselect</ListItem>
        <ListItem>useQuery</ListItem>
        <ListItem>useMutation</ListItem>
      </List>
      <List>
        <b>React-Hook-Form:</b>
        <ListItem>useFieldArray</ListItem>
        <ListItem>useWatch</ListItem>
        <ListItem>useFieldState</ListItem>
      </List>
      <List>
        <b>i18n:</b>
        <ListItem>типизация + автокомплит</ListItem>
        <ListItem>переключение языка</ListItem>
      </List>
      <List>
        <b>Mui:</b>
        <ListItem>gitlab-sidebar clone</ListItem>
        <ListItem>кастомизация темы</ListItem>
        <ListItem>переключение темы</ListItem>
      </List>
      <List>
        <b>Date-fns:</b>
        <ListItem>локализация (таймзоны)</ListItem>
      </List>
      <List>
        <b>и многое другое...</b>
      </List>
    </div>
  </Box>
);
