require("dotenv").config();
const { merge } = require("webpack-merge");
const common = require("./common.js");
const TerserPlugin = require("terser-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ModuleFederationPlugin =
  require("webpack").container.ModuleFederationPlugin;
const path = require("path");
const commonModuleFederationConfig = require("./mf-config.js");
const CompressionPlugin = require("compression-webpack-plugin");
const { srcPath } = require("./path");

// проблемно настраивается с wmf
// const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

const {
  PROD_API = "http://makarenkoff.ru",
  FE_SHARED_UI_PORT = 8082,
  FE_AUTH_PORT = 8083,
  FE_HOST_PORT = 8084,
} = process.env;

module.exports = ({ ANALYZE }) => {
  const plugins = [
    new HtmlWebpackPlugin({
      // добавление хэша для кэширования
      hash: true,
      inject: true,
      template: path.join(srcPath, "index.html"),
      minify: {
        // it's all default options
        collapseWhitespace: true,
        keepClosingSlash: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        removeComments: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        // https://github.com/jantimon/html-webpack-plugin#minification
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true,
      },
    }),
    new ModuleFederationPlugin({
      ...commonModuleFederationConfig,
      remotes: {
        host: `host@${PROD_API}:${FE_HOST_PORT}/remoteEntry.js`,
        auth: `auth@${PROD_API}:${FE_AUTH_PORT}/remoteEntry.js`,
        shared_ui: `shared_ui@${PROD_API}:${FE_SHARED_UI_PORT}/remoteEntry.js`,
      },
    }),
    new CompressionPlugin(),
  ];

  return merge(common, {
    mode: "production",
    // Recommended choice for production builds with high quality SourceMaps.
    devtool: "source-map",
    output: {
      publicPath: `${PROD_API}:${FE_HOST_PORT}/`,
    },
    optimization: {
      // tells webpack whether to conduct inner graph analysis for unused exports.
      innerGraph: true,
      // Short names - usually a single char - focused on minimal download size.
      mangleExports: "size",
      // reduce the size of WASM by changing imports to shorter strings. It mangles module and export names.
      mangleWasmImports: true,
      // Module names are hashed into small numeric values
      moduleIds: "deterministic",
      // Tells webpack to figure out which exports are provided by modules to generate more efficient code for export * from ....
      // By default optimization.providedExports is enabled.
      providedExports: true,
      // Tells webpack to detect and remove modules from chunks when these modules are already included in all parents.
      removeAvailableModules: true,
      // Tell webpack to minimize the bundle using the TerserPlugin or the plugin(s) specified in optimization.minimizer.
      minimize: true,
      minimizer: [
        new TerserPlugin({
          terserOptions: {
            compress: true,
            format: {
              comments: false,
            },
          },
          extractComments: true,
        }),
      ],
    },
    plugins: plugins,
    stats: {
      usedExports: true,
      providedExports: true,
      env: true,
    },
    output: {
      clean: true,
    },
    performance: {
      hints: false,
    },
  });
};
