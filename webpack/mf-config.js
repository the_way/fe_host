const deps = require("../package.json").dependencies;

const commonModuleFederationConfig = {
    name: "host",
    filename: "remoteEntry.js",
    shared: {
        // просто мапить deps
        // для кор библиотек
        ...deps,
        react: {
            singleton: true,
            requiredVersion: deps.react,
        },
        "react-dom": {
            singleton: true,
            requiredVersion: deps["react-dom"],
        },
        "react-router-dom": { requiredVersion: deps["react-router-dom"], singleton: true },
    },

}

module.exports = commonModuleFederationConfig