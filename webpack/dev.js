require('dotenv').config();
const path = require('path');
const { merge } = require('webpack-merge');
const ForkTsCheckerNotifierWebpackPlugin = require('fork-ts-checker-notifier-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CircularDependencyPlugin = require('circular-dependency-plugin');
const ModuleFederationPlugin =
    require("webpack").container.ModuleFederationPlugin;
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');
const deps = require("../package.json").dependencies;

const common = require('./common.js');
const commonModuleFederationConfig = require('./mf-config.js');
const { rootPath, staticPath, distPath, srcPath } = require('./path');

const { DEV_API = 'http://localhost',
    FE_SHARED_UI_PORT = 8082,
    FE_AUTH_PORT = 8083,
    FE_HOST_PORT = 8084 } = process.env

module.exports = merge(common, {
    mode: 'development',
    // при auto react-router не находит страницы
    output: {
        publicPath: `${DEV_API}:${FE_HOST_PORT}/`,
    },
    // Choose a style of source mapping to enhance the debugging process. These values can affect build and rebuild speed dramatically.
    // Recommended choice for development builds with maximum performance.
    devtool: 'eval',

    resolve: {
        symlinks: false,
        cacheWithContext: false,
    },

    cache: {
        type: 'filesystem',
        cacheDirectory: path.join(rootPath, '.webpack_cache'),
        buildDependencies: {
            config: [__filename],
        },
    },

    stats: 'errors-only',

    devServer: {
        port: FE_HOST_PORT,
        historyApiFallback: true,
    },
    plugins: [
        new ForkTsCheckerNotifierWebpackPlugin({
            excludeWarnings: true,
        }),

        new CircularDependencyPlugin({
            exclude: /a\.js|node_modules/,
            include: /dir/,
            failOnError: true,
            allowAsyncCycles: false,
            cwd: process.cwd(),
        }),

        new HtmlWebpackPlugin({
            // Passing true will add it to the head/body depending on the scriptLoading option. 
            inject: true,
            template: path.join(srcPath, 'index.html'),
        }),

        new ModuleFederationPlugin({
            ...commonModuleFederationConfig,
            remotes: {
                host: `host@${DEV_API}:${FE_HOST_PORT}/remoteEntry.js`,
                auth: `auth@${DEV_API}:${FE_AUTH_PORT}/remoteEntry.js`,
                shared_ui: `shared_ui@${DEV_API}:${FE_SHARED_UI_PORT}/remoteEntry.js`,
            },
        }),
    ],
    optimization: {
        // не работает с WMF
        // runtimeChunk: true,

        removeAvailableModules: false,
        removeEmptyChunks: false,
        splitChunks: false,
    },
});
