require("dotenv").config();
const path = require("path");
const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");
const Dotenv = require("dotenv-webpack");
const { srcPath } = require("./path");
module.exports = {
  entry: path.join(srcPath, "index"),

  resolve: {
    alias: {
      app: path.join(srcPath, "app"),
      processes: path.join(srcPath, "processes"),
      pages: path.join(srcPath, "pages"),
      widgets: path.join(srcPath, "widgets"),
      features: path.join(srcPath, "features"),
      entities: path.join(srcPath, "entities"),
      shared: path.join(srcPath, "shared"),
    },

    // идёт в порядке приоритета, что первее грузить при импорте
    extensions: [".ts", ".tsx", ".js"],
    // plugins: [new TsconfigPathsPlugin()],
  },

  module: {
    rules: [
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.tsx?$/,
        loader: "babel-loader",
        exclude: /node_modules/,
        options: {
          presets: [
            [
              "@babel/preset-react",
              // без этого - ошибка 'react isn't defined
              {
                runtime: "automatic",
              },
            ],
            "@babel/preset-typescript",
          ],
        },
      },
    ],
  },

  plugins: [new Dotenv()],
};
